#
# 3DS Build Image
#
# Holger Zahnleiter, 2021-04-16
#

.PHONY: image push

.EXPORT_ALL_VARIABLES:
REGISTRY := registry.gitlab.com/$(or $(CI_PROJECT_NAMESPACE),hzahnlei)/$(or $(CI_PROJECT_NAME),3ds-build-image)
BASE_IMAGE := debian
BASE_IMAGE_TAG := buster-slim
TARGET_IMAGE_NAME := 3ds-build-image
TARGET_IMAGE_TAG := 1.0.0
TARGET_IMAGE_FULL_NAME := $(REGISTRY)/$(TARGET_IMAGE_NAME):$(TARGET_IMAGE_TAG)

image:
	# docker login registry.gitlab.com
	docker build -f Dockerfile --build-arg BASE_IMAGE --build-arg BASE_IMAGE_TAG --build-arg TARGET_IMAGE_TAG -t $(TARGET_IMAGE_FULL_NAME) .
	# docker push $(TARGET_IMAGE_FULL_NAME)

push: image
	echo "${CI_REGISTRY_PASSWORD}" | docker login --username "${CI_REGISTRY_USER}" --password-stdin registry.gitlab.com
	docker push $(TARGET_IMAGE_FULL_NAME)
